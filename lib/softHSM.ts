import {
  KeyGenMechanism,
  KeyType,
  Module,
  PrivateKey,
  Session,
  SessionFlag,
  Slot,
  SlotFlag,
  TokenFlag,
  UserType
} from 'graphene-pk11';
import { defaults } from 'lodash/fp';
import * as NodeRSA from 'node-rsa';

import { createSilentLogger, Logger } from './logger';

export interface SoftHSMOptions {
  pin: string;
  libraryPath: string;
  privateKeyLabel?: string;
  publicKeyLabel?: string;
  tokenLabel?: string;
  logger?: Logger;
}

export class SoftHSM {
  private mod: Module;
  private options: Required<SoftHSMOptions>;

  constructor(options: SoftHSMOptions) {
    this.options = this.parseOptions(options);
    this.mod = Module.load(this.options.libraryPath, 'SoftHSM');
  }

  private parseOptions(options: SoftHSMOptions): Required<SoftHSMOptions> {
    return defaults(
      {
        privateKeyLabel: 'LGO#PrivateKey',
        publicKeyLabel: 'LGO#PublicKey',
        tokenLabel: 'LGO#Slot',
        logger: createSilentLogger()
      },
      options
    );
  }

  public initialize(): void {
    this.createSlotIfNeeded();
    this.createKeyPairIfNeeded();
  }

  private createSlotIfNeeded(): void {
    if (this.isSlotCreated()) {
      this.options.logger.log('HSM slot already created');
    } else {
      this.options.logger.log('Creating HSM slot');
      this.createSlot();
    }
  }

  private isSlotCreated(): boolean {
    try {
      this.mod.initialize();
      return this.findLgoSlot() !== undefined;
    } catch (e) {
      return false;
    } finally {
      this.finalizeModSilently();
    }
  }

  private createSlot(): void {
    try {
      this.mod.initialize();
      const slot = this.mod.getSlots(0);
      // @ts-ignore
      slot.initToken(this.options.pin, this.options.tokenLabel);
      const session = slot.open(
        SessionFlag.RW_SESSION | SessionFlag.SERIAL_SESSION
      );
      session.login(this.options.pin, UserType.SO);
      session.initPin(this.options.pin);
      session.logout();
      session.close();
    } finally {
      this.finalizeModSilently();
    }
  }

  public createKeyPairIfNeeded(): void {
    this.openClose(session => {
      const publicKeys = session.find({ label: this.options.publicKeyLabel });
      if (publicKeys.length === 0) {
        this.options.logger.log('Creating key pair in HSM slot');
        this.createKeyPair(session);
      } else {
        this.options.logger.log('Key pair already present in HSM slot');
      }
    });
  }

  public sign(data: string): string {
    return this.openClose(session => {
      const privateKey: PrivateKey = session
        .find({ label: this.options.privateKeyLabel })
        .items(0)
        .toType();
      const sign = session.createSign('SHA256_RSA_PKCS', privateKey);
      sign.update(data);
      const signature = sign.final();
      return signature.toString('hex');
    });
  }

  public getPubKey(): string | undefined {
    if (!this.isSlotCreated()) {
      return undefined;
    }
    return this.openClose((session: Session) => {
      const pubKey = session
        .find({ label: this.options.publicKeyLabel })
        .items(0)
        .toType()
        .getAttribute({
          modulus: null,
          publicExponent: null
        });
      if (!pubKey.modulus) {
        return undefined;
      }
      return new NodeRSA()
        .importKey(
          {
            n: pubKey.modulus,
            e: 3
          },
          'components-public'
        )
        .exportKey('pkcs8-public');
    });
  }

  private findLgoSlot(): Slot | undefined {
    const slots = this.mod.getSlots();
    for (let i = 0; i < slots.length; i++) {
      const slot = slots.items(i);
      if (this.tokenPresent(slot) && this.tokenInitialized(slot)) {
        return slot;
      }
    }
    return undefined;
  }

  private tokenPresent(slot: Slot): boolean {
    return (slot.flags & SlotFlag.TOKEN_PRESENT) !== 0;
  }

  private tokenInitialized(slot: Slot): boolean {
    const token = slot.getToken();
    return (
      token.label === this.options.tokenLabel &&
      (token.flags & TokenFlag.TOKEN_INITIALIZED) !== 0
    );
  }

  private createKeyPair(session: Session): void {
    session.generateKeyPair(
      KeyGenMechanism.RSA,
      {
        keyType: KeyType.RSA,
        modulusBits: 2048,
        publicExponent: Buffer.from([3]),
        token: true,
        verify: true,
        encrypt: true,
        wrap: true,
        label: this.options.publicKeyLabel
      },
      {
        keyType: KeyType.RSA,
        token: true,
        sign: true,
        decrypt: true,
        unwrap: true,
        label: this.options.privateKeyLabel
      }
    );
  }

  private openClose<TResult>(func: (session: Session) => TResult): TResult {
    try {
      this.mod.initialize();
      const slot = this.findLgoSlot();
      if (!slot) {
        throw new Error('Not initialized');
      }
      const session = slot.open(
        SessionFlag.RW_SESSION | SessionFlag.SERIAL_SESSION
      );
      this.tryLogin(session);
      const result = func(session);
      session.logout();
      session.close();
      return result;
    } finally {
      this.finalizeModSilently();
    }
  }

  private tryLogin(session: Session): void {
    try {
      session.login(this.options.pin);
    } catch (e) {
      if (e.message.includes('CKR_PIN_INCORRECT')) {
        throw new Error('HSM pin is incorrect');
      }
      const [firstLine] = (e.message || '').split('\n');
      throw new Error(firstLine);
    }
  }

  private finalizeModSilently(): void {
    try {
      this.mod.finalize();
    } catch (error) {
      this.options.logger.error(`Impossible to finalize mod: ${error.message}`);
    }
  }
}
