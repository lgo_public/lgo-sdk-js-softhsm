export interface Logger {
  log: (message: string) => void;
  error: (message: string) => void;
}

export function createSilentLogger(): Logger {
  return {
    log: () => undefined,
    error: () => undefined
  };
}
