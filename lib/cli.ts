#!/usr/bin/env node

// tslint:disable:no-console
import * as yargs from 'yargs';

import { SoftHSM, SoftHSMOptions } from './softHSM';

interface AllOptions {
  [key: string]: yargs.Options;
}

const allOptions: AllOptions = {
  'library-path': {
    describe: 'library path',
    demand: true,
    type: 'string'
  },
  pin: {
    describe: 'pin',
    demand: true,
    type: 'string'
  }
};

yargs
  .command(
    'info',
    'display the public key stored in hsm',
    builder => builder.options(allOptions),
    argv => infoCommand(argvToOptions(argv))
  )
  .command(
    'init',
    'initialize the hsm',
    builder => builder.options(allOptions),
    argv => initializeCommand(argvToOptions(argv))
  )
  .parse();

function infoCommand(options: SoftHSMOptions) {
  const softHSM = new SoftHSM(options);
  const publicKey = softHSM.getPubKey();
  if (!publicKey) {
    console.log('HSM not initialized. Please use init command.');
  } else {
    console.log(publicKey);
  }
}

function initializeCommand(options: SoftHSMOptions) {
  const softHSM = new SoftHSM(options);
  softHSM.initialize();
}

function argvToOptions(argv: any): SoftHSMOptions {
  return {
    libraryPath: argv['library-path'] as string,
    pin: argv.pin as string,
    logger: console
  };
}
