import { CryptoKi } from './cryptoKi';
import { SoftHSM, SoftHSMOptions } from './softHSM';

export class SoftHSMCryptoKi implements CryptoKi {
  private softHSM: SoftHSM;

  constructor(options: SoftHSMOptions) {
    this.softHSM = new SoftHSM(options);
  }

  public async sign(data: string) {
    return this.softHSM.sign(data);
  }
}
