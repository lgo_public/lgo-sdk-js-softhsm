# @lgo/sdk-softhsm

SoftHSM CryptoKi plugin for [LGO SDK](https://gitlab.com/lgo_public/lgo-sdk-js).

## Requirements

This plugin is based on [SoftHSM](https://www.opendnssec.org/softhsm) and needs some tooling.

### Linux

Examples are based on Ubuntu.

Install:

- wget
- build-essential (or enough tooling to compile)
- libssl-dev (OpenSSL headers, you can also use Botan's one)

Install SoftHSM:

Note: the example below uses SoftHSM 2.5.0.

```
wget https://dist.opendnssec.org/source/softhsm-2.5.0.tar.gz
tar -xzf softhsm-2.5.0.tar.gz
cd softhsm-2.5.0
./configure --disable-gost
make
sudo make install
```

Configuration locations are:

- configuration: `/usr/local/lib/softhsm/softhsm2.conf`
- tokens: `/var/lib/softhsm/tokens`
- library path: `/usr/local/lib/softhsm/libsofthsm2.so`

### macOS

Install:

- [brew](https://brew.sh)
- [nvm](https://github.com/creationix/nvm)

Install SoftHSM:

```
brew install softhsm
```

Configuration locations are:

- configuration: `/usr/local/etc/softhsm/softhsm2.conf`
- tokens: `/usr/local/var/lib/softhsm/tokens`
- library path: `/usr/local/Cellar/softhsm/2.5.0/lib/softhsm/libsofthsm2.so`

## Installation

First install the npm dependency:

```
npm install @lgo/sdk-softhsm --save
```

Then, initialize the SoftHSM token if it's your first installation:

```
./node_modules/.bin/lgo-sdk-softhsm init --library-path <softhsm_library_path> --pin <your_pin>
```

You can retrieve your public key and send it via LGO desktop app:

```
./node_modules/.bin/lgo-sdk-softhsm info --library-path <softhsm_library_path> --pin <your_pin>
```

## Example

```ts
import { Client } from '@lgo/sdk';
import { SoftHSMCryptoKi } from '@lgo/sdk-softhsm';

const libraryPath = '< softhsm libary path >';
const pin = '< softhsm pin >';

const cryptoKi = new SoftHSMCryptoKi({ libraryPath, pin });

const client = new Client({
  cryptoKi
  /* other options */
});

client.getMyOrders().then(console.log);
```

## License

[MIT](LICENSE)
